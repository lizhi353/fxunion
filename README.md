#### fxunion的使用
```shell
# import引入当前的包，然后执行：
$ go mod tidy
# 更新包，记住提交代码前更新一下包文件，此包为内部包，不定期的会更新的。
$ go get -u gitee.com/lizhi353/fxunion

$ go test -v 单元测试的方法名 #执行前先cd tests目录
```
#### 静态库声明

| 文件名（包）                          | 描述         | golang版本 |
| ------------------------------------- | ------------ | ---------- |
| gitee.com/lizhi353/fxunion/redigopack | redis使用包  | 1.18       |
| gitee.com/lizhi353/fxunion/pagination | gorm分页     | 1.18       |
| gitee.com/lizhi353/fxunion/restyhttp  | http接口调用 | 1.18       |
| gitee.com/lizhi353/fxunion/loghoop    | 云日志       | 1.18       |
| gitee.com/lizhi353/fxunion/tests      | 单元测试     | 1.18       |

如golang版本在1.18以下或代码的版本低于1.18，可联系本人注意升级当前项目的版本库升级



#### 代码里引入

```go
//如引入redigopack模块
import (
    "gitee.com/lizhi353/fxunion/redigopack"
)
//.... fxunion子模块较多，这里import只用标注具体的子模块即可，不必全部引入

```

#### 实例

1. redis使用

   ```go
   import (
    "gitee.com/lizhi353/fxunion/redigopack"
   )
     server := "127.0.0.1:6379"
   	pwd := ""//如无密码，请放空字符串
    db := 2 //数据库名，如不选择，默认值为0
   //建立redis链接
   	redisPool := redigopack.InitRedis(server, pwd, db)
   //获取redis客户端对象
   redigopack.InitRedisClient(redisPool, "pre_")
   //关闭对象dia对象
   	defer redisPool.ConnPool.Close()
   //尝试给aa这个键赋值
   	redigopac.GetRedisClient().String.Set("aa",22211, int64(60))
   //获取aa的值
     aa,_ := redisClient.String.Get("aa").String()
     fmt.Printf("redis key aa value:%s\n", aa)
   ```



2. 分页使用

   ```go
   import (
    "gitee.com/lizhi353/fxunion/pagination"
   )
   
   func MapList(ctx *gin.Context)  {
   	var newList []*HomeList
   	query := model.GetNewQueryByTypeId()
   	page := ctx.Query("page")
   	pageInt,_ := strconv.Atoi(page)
   	paginationObj := pagination.InitPagination(10, pageInt,  query)//加载分页,参数： pagesize、当前page、gorm的*gorm.DB
   	err := paginationObj.GetList(&newList)//绑定列表
   	if err != nil {
   		logrus.Error(err)
   		ResponseException("加载列表失败", ctx)
   	}
   	ctx.JSON(http.StatusOK, gin.H{"newList":newList,"pageCount":paginationObj.GetPageCount(), "page": paginationObj.GetPage()})
   }
   ```



3. http接口，引用接口自动引入中间件

   （1）post接口
   
   ```go
   type PostRes struct {
   	PageNum  int     `json:"page_num"`
   	PageSize int     `json:"page_size"`
   	Total    int     `json:"total"`
   	Pages    int     `json:"pages"`
   	List     []Cimsi `json:"list"`
   }
   
   type Cimsi struct {
   	Id          string `json:"id"`
   	Iccid       string `json:"iccid"`
   	Imsi        string `json:"imsi"`
   	Msisdn      string `json:"msisdn"`
   	TaskId      string `json:"task_id"`
   	Date        int    `json:"date"`
   	SimState    string `json:"sim_state"`
   	FlowPackage string `json:"flow_package"`
   	Vin         string `json:"vin"`
   	TboxSN      string `json:"tbox_sn"`
   	DetailTip   string `json:"detailTip"`
   }
   
   func main()  {
   	restyhttp.InitRestClient()
   	simgParams := map[string]interface{}{
   		"vin": "LB9AB2AC8J0LDN227",
   		"pager": map[string]interface{}{
   			"order":"desc",
   			"pageNum":0,
   			"pageSize":10,
   		},
   	}
   	postRes := &PostRes{}
   	url := "http://localgw.test.cloud.enovatemotors.com/vum/sim/internal/admin/infos"
   	ctx := context.Background()
   	err := restyhttp.GetRestyClient().HttpPostReq(ctx, url, simgParams, postRes)
   	fmt.Printf("ctx-----result: %+v, err:%v", postRes, err)
   }
   ```

（2）get接口

```go
type EcallMsdResult struct {
	List []EcallMsd `json:"list"`
	Total int       `json:"total"`
}
type EcallMsd struct {
	CallerId int `json:"caller_id"`
	CallerName string `json:"caller_name"`
	AbilityId int `json:"ability_id"`
	AbilityName string `json:"ability_name"`
	BusinessCode int `json:"business_code"`
	BusinessName string `json:"business_name"`
	BusinessEnable int `json:"business_enable"`
	MouldId int `json:"mould_id"`
	MouldContent string `json:"mould_content"`
	MouldName string `json:"mould_name"`
	ChannelId int `json:"channel_id"`
	ChannelName string `json:"channel_name"`
	CalleType int `json:"calle_type"`
	Description string `json:"description"`
	CreateTime string `json:"create_time"`
	UpdateTime string `json:"update_time"`
	UpdateBy string `json:"update_by"`
	AccessKey string `json:"access_key"`
	Enable int `json:"enable"`
	Limit int `json:"limit"`
	MustArrived string `json:"must_arrived"`
}
type BusinessResult struct {
	Code        int            `json:"code"`
	Message     string         `json:"message"`
	BusinessObj EcallMsdResult `json:"businessObj"`

}
func main()  {
	restyhttp.InitRestClient()
	url := "http://localgw.test.cloud.enovatemotors.com/push/business/list"
	body := make(map[string]string, 0)
	body["business_code"] = "1706"
	rusinessResult := &BusinessResult{}
	ctx := context.Background()
	err := restyhttp.GetRestyClient().HttpGetReq(ctx, url, body, rusinessResult)
	fmt.Printf("result is info:%+v, err:%v", rusinessResult, err)
}
```

相关单元测试参考：tests/restry_test.go

4、golang云日志

```go
type Result struct {
	Message string `json:"message"`
}

func main()  {
	loghoop.SetUp("ecall")
	restyhttp.InitRestClient()
	r := gin.Default()

	r.Use(loghoop.AccessLog()).GET("/ping", func(c *gin.Context) {
		body := map[string]string{
			"a":"1",
		}
		ctx := context.Background()
		result := &Result{}
		err := restyhttp.GetRestyClient().HttpGetReq(ctx, "http://127.0.0.1:8080/json", body, result)
		if err != nil {
			panic(err)
		}
		loghoop.Infof("------:he:%v", 111112222)
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.GET("/json", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message":"json to be",
		})
	})
	r.Run() // l
}
```



执行go run的，这里判断了第2个参数，也就是 go run main.go 第二个参数

如果为空或者为dev就在本地创建一个logs文件夹，文件夹中写入文件日志，restyhttp请求http的时候会自动进行写入

