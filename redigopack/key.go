package redigopack

import (
	"github.com/gomodule/redigo/redis"
)

type keyRds struct {
	*RedisPool
}

// 	查找键 [*模糊查找]
func (k *keyRds) Keys(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("keys", kname))
}

// 	判断key是否存在
func (k *keyRds) Exists(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("exists", kname))
}

// 随机返回一个key
func (k *keyRds) RandomKey() *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("randomkey"))
}

// 返回值类型
func (k *keyRds) Type(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("type", kname))
}

// 删除key
func (k *keyRds) Del(keys ...string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	prekeys := GetRedisClient().SlicePreName(keys)
	return getReply(c.Do("del", redis.Args{}.AddFlat(prekeys)...))
}

//重命名
func (k *keyRds) Rename(key, newKey string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("rename", kname, newKey))
}

// 仅当newkey不存在时重命名
func (k *keyRds) RenameNX(key, newKey string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("renamenx", kname, newKey))
}

//	序列化key
func (k *keyRds) Dump(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("dump", kname))
}

// 反序列化
func (k *keyRds) Restore(key string, ttl, serializedValue interface{}) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("restore", kname, ttl, serializedValue))
}

// 秒
func (k *keyRds) Expire(key string, seconds int64) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("expire", kname, seconds))
}

// 秒
func (k *keyRds) ExpireAt(key string, timestamp int64) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("expireat", kname, timestamp))
}

// 毫秒
func (k *keyRds) Persist(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("persist", kname))
}

// 毫秒
func (k *keyRds) PersistAt(key string, milliSeconds int64) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("persistat", kname, milliSeconds))
}

// 秒
func (k *keyRds) TTL(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("ttl", kname))
}

// 毫秒
func (k *keyRds) PTTL(key string) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("pttl", kname))
}

//	同实例不同库间的键移动
func (k *keyRds) Move(key string, db int64) *Reply {
	c := k.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("move", kname, db))
}
