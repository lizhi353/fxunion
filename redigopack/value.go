package redigopack
import (
	"errors"
	"fmt"

	"github.com/gomodule/redigo/redis"
)

type valuesRds struct {
	*RedisPool
}

//// key不存在是在设置值
func (s *valuesRds) SetNX(key string, value interface{}, expire int64) bool {
	c := s.ConnPool.Get()
	defer c.Close()
	re, err := getReply(c.Do("setnx", key, value)).Int64()

	if err != nil {
		panic(err)
	}
	exRe, _ := getReply(c.Do("expire", key, expire)).Int()
	fmt.Println(exRe)
	return re == 1
}

//redis->get获取int类型数据
func (s *valuesRds) GetInt(key string) int {
	c := s.ConnPool.Get()
	defer c.Close()
	res, err := getReply(c.Do("get", key)).Int()
	if err != nil {
		if errors.Is(err, redis.ErrNil) {
			return 0
		}
		panic(err)
	}
	return res
}

//redis->get获取string类型数据
func (s *valuesRds) GetString(key string) string {
	c := s.ConnPool.Get()
	defer c.Close()
	res, err := getReply(c.Do("get", key)).String()
	if err != nil {
		if errors.Is(err, redis.ErrNil) {
			return ""
		}
		panic(err)
	}
	return res
}
