package redigopack

type listRds struct {
	*RedisPool
}

//向列表头插入元素
func (l *listRds) LPush(key string, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lpush", kname, value))
}

//当列表存在则将元素插入表头
func (l *listRds) LPushx(key string, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lpushx", kname, value))
}

//将指定元素插入列表末尾
func (l *listRds) RPush(key string, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("rpush", kname, value))
}

//当列表存在则将元素插入表尾
func (l *listRds) RPushx(key string, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("rpushx", kname, value))
}

//将元素插入指定位置position:BEFORE|AFTER,当 pivot 不存在于列表 key 时，不执行任何操作。当 key 不存在时， key 被视为空列表，不执行任何操作。
func (l *listRds) LInsert(key, position, pivot, value string) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("linsert", kname, position, pivot, value))
}

//返回列表头元素
func (l *listRds) LPop(key string) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lpop", kname))
}

//阻塞并弹出头元素
func (l *listRds) BLpop(key string, timeout interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("blpop", kname, timeout))
}

//返回列表尾元素
func (l *listRds) RPop(key string) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("rpop", kname))
}

//阻塞并弹出末尾元素
func (l *listRds) BRpop(key string, timeout interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("brpop", kname, timeout))
}

//返回指定位置的元素
func (l *listRds) LIndex(key string, index interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lindex", kname, index))
}

//获取指定区间的元素
func (l *listRds) LRange(key string, start, stop interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lrange", kname, start, stop))
}

//设置指定位元素
func (l *listRds) LSet(key string, index, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lset", kname, index, value))
}

//弹出source尾元素并返回，将弹出元素插入destination列表的开头
func (l *listRds) RPoplpush(key, source, destination string) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("rpoplpush ", kname, source, destination))
}

//阻塞并弹出尾元素，将弹出元素插入另一列表的开头
func (l *listRds) BRpoplpush(key, source, destination string, timeout interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("brpoplpush ", kname, source, destination, timeout))
}

//移除元素,count = 0 : 移除表中所有与 value 相等的值,count!=0,移除与 value 相等的元素，数量为 count的绝对值
func (l *listRds) LRem(key string, count, value interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("lrem", kname, count, value))
}

//列表裁剪，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除。-1 表示尾部
func (l *listRds) LTrim(key string, start, stop interface{}) *Reply {
	c := l.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("ltrim", kname, start, stop))
}
