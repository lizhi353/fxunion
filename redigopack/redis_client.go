package redigopack

type RedisClient struct {
	String    *stringRds
	List      *listRds
	Hash      *hashRds
	Key       *keyRds
	Set       *setRds
	ZSet      *zSetRds
	Bit       *bitRds
	Db        *dbRds
	ValuesRds *valuesRds
	Prefix string
}

var redisPackClient *RedisClient

func InitRedisClient(pool *RedisPool, prefix string) *RedisClient {
	redisPackClient =  &RedisClient{
		String:    &stringRds{pool},
		List:      &listRds{pool},
		Hash:      &hashRds{pool},
		Key:       &keyRds{pool},
		Set:       &setRds{pool},
		ZSet:      &zSetRds{pool},
		Bit:       &bitRds{pool},
		Db:        &dbRds{pool},
		ValuesRds: &valuesRds{pool},
		Prefix: prefix,
	}
	return redisPackClient
}

func GetRedisClient() *RedisClient {
	return redisPackClient
}


