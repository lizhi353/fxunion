package redigopack

import (
	"github.com/gomodule/redigo/redis"
)

type zSetRds struct {
	*RedisPool
}

// map[score]member  添加元素
func (z *zSetRds) ZAdd(key string, mp map[interface{}]interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zadd", redis.Args{}.Add(key).AddFlat(mp)...))
}

// 	增加元素权重
func (z *zSetRds) ZUncrBy(key string, increment, member interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zuncrby", key, increment, member))
}

// 	增加元素权重
func (z *zSetRds) ZCard(key string) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zcard", key))
}

// 	返回指定元素的排名
func (z *zSetRds) ZEank(key string, member interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zrank", key, member))
}

// 	返回指定元素的权重
func (z *zSetRds) ZScore(key string, member interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zscore", key, member))
}

// 返回集合两个权重间的元素数
func (z *zSetRds) ZCount(key string, min, max interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zcount", key, min, max))
}

// 返回指定区间内的元素
func (z *zSetRds) ZRange(key string, start, stop interface{}, withScore ...bool) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	if len(withScore) > 0 && withScore[0] {
		return getReply(c.Do("zrange", key, start, stop, "WITHSCORES"))
	}
	return getReply(c.Do("zrange", key, start, stop))
}

// 倒序返回指定区间内的元素
func (z *zSetRds) ZRevrange(key string, start, stop interface{}, withScore ...bool) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	if len(withScore) > 0 && withScore[0] {
		return getReply(c.Do("zrevrange", key, start, stop, "WITHSCORES"))
	}
	return getReply(c.Do("zrevrange", key, start, stop))
}

// 按照score区间返回元素
func (z *zSetRds) ZRangeByScore(key string, min, max interface{}, withScore ...bool) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	if len(withScore) > 0 && withScore[0] {
		return getReply(c.Do("ZRANGEBYSCORE", key, min, max, withScore))
	}
	return getReply(c.Do("ZRANGEBYSCORE", key, min, max))
}

// 删除元素
func (z *zSetRds) ZRem(key string, member interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zrem", key, member))
}

//删除指定的元素
func (z *zSetRds) ZRemMembers(key string, members []interface{}) *Reply {
	c := z.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("zrem", redis.Args{}.Add(key).AddFlat(members)...))

}
