package redigopack

import (
	"github.com/gomodule/redigo/redis"
)

type hashRds struct {
	*RedisPool
}

//exist 为true 表示字段不存则设置其值
func (h *hashRds) HSet(key string, filed, value interface{}, exist ...bool) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	if len(exist) > 0 && exist[0] {
		return getReply(c.Do("hsetex", kname, filed, value))
	}
	return getReply(c.Do("hset", kname, filed, value))
}

//获取指定字段值
func (h *hashRds) HGet(key string, filed interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hget", kname, filed))
}

//获取所有字段及值
func (h *hashRds) HGetAll(key string) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hgetall", kname))
}

//设置多个字段及值 [map]
func (h *hashRds) HMSetFromMap(key string, mp map[interface{}]interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hmset", redis.Args{}.Add(kname).AddFlat(mp)...))
}

//设置多个字段及值 [struct]
func (h *hashRds) HMSetFromStruct(key string, obj interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hmset", redis.Args{}.Add(kname).AddFlat(obj)...))
}

//返回多个字段值
func (h *hashRds) HMGet(key string, fileds interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hmget", redis.Args{}.Add(kname).AddFlat(fileds)...))
}

//字段删除
func (h *hashRds) HDel(key string, fileds interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hdel", redis.Args{}.Add(kname).AddFlat(fileds)...))
}

//判断字段是否存在
func (h *hashRds) HExists(key string, filed interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hexists", kname, filed))
}

//返回所有字段
func (h *hashRds) HKeys(key string) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hkeys", kname))
}

//返回字段数量
func (h *hashRds) HLen(key string) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hlen", kname))
}

//返回所有字段值
func (h *hashRds) HVals(key string) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hvals", kname))
}

//为指定字段值增加
func (h *hashRds) HIncrBy(key string, filed interface{}, increment interface{}) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hincrby", kname, filed, increment))
}

//为指定字段值增加浮点数
func (h *hashRds) HIncrByFloat(key string, filed interface{}, increment float64) *Reply {
	c := h.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("hincrbyfloat", kname, filed, increment))
}
