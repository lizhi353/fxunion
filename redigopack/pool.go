package redigopack

import (
	red "github.com/gomodule/redigo/redis"
	"time"
)

type RedisPool struct {
	ConnPool *red.Pool
}

// InitRedis 初始化redis链接
func InitRedis(server, pwd string, db int) *RedisPool {
	cacheConn := RedisPool{&red.Pool{
		MaxIdle:     10, //最大的空闲连接数
		MaxActive:   200,//最大连接数
		IdleTimeout: time.Duration(120),
		Dial: func() (red.Conn, error) {
			dial,err := red.Dial(
				"tcp",
				server,
				red.DialReadTimeout(time.Duration(1000)*time.Millisecond),
				red.DialPassword(pwd),
				red.DialWriteTimeout(time.Duration(1000)*time.Millisecond),
				red.DialConnectTimeout(time.Duration(1000)*time.Millisecond),
			)
			if err != nil {
				panic(err)
			}
			if db > 0 {
				dial.Do("SELECT", db)
			}
			return dial,err
		},
	},
	}
	// connection = &cacheConn
	return &cacheConn
}