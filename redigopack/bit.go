package redigopack

import (
	red "github.com/gomodule/redigo/redis"
)

type bitRds struct {
	*RedisPool
}

func (b *bitRds) SetBit(key string, offset, value int64) *Reply {
	c := b.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("setbit", kname, offset, value))
}

func (b *bitRds) GetBit(key string, offset int64) *Reply {
	c := b.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	return getReply(c.Do("getbit", kname, offset))
}

func (b *bitRds) BitCount(key string, interval ...int64) *Reply {
	c := b.ConnPool.Get()
	defer c.Close()
	kname := GetRedisClient().GetPrefixKey(key)
	if len(interval) == 2 {
		return getReply(c.Do("bitcount", kname, interval[0], interval[1]))
	}
	return getReply(c.Do("bitcount", kname))
}

// opt 包含 and、or、xor、not
func (b *bitRds) BitTop(opt, destKey string, keys ...string) *Reply {
	c := b.ConnPool.Get()
	defer c.Close()
	knames := GetRedisClient().SlicePreName(keys)
	return getReply(c.Do("bitop", opt, red.Args{}.Add(knames).AddFlat(keys)))
}
