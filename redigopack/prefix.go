package redigopack

//获取key名称
func (rc *RedisClient) GetPrefixKey(key string) string {
	return rc.Prefix + key
}

/**
** 添加多个值的时候批量赋值
 */
func (rc *RedisClient) MPreName(kv map[string]interface{}) map[string]interface{}  {
	kvM2 := make(map[string]interface{}, 0)
	if len(kv) > 0 {
		for k,v := range kv {
			key := rc.GetPrefixKey(k)
			kvM2[key] = v
		}
		return kvM2
	}
	return kvM2
}

func (rc *RedisClient) SlicePreName(keys []string) []string {
	keysPre := make([]string, 0)
	for _,v := range keys {
		keysPre = append(keysPre, v)
	}
	return keysPre
}