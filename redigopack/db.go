package redigopack

type dbRds struct {
	*RedisPool
}

func (d *dbRds) SelectDb(db int64) *Reply {
	c := d.ConnPool.Get()
	defer c.Close()
	return getReply(c.Do("select", db))
}
