/**
**  $ go test -v -run RedisPackClient redigo_test.go
 */
package tests

import (
	"gitee.com/lizhi353/fxunion/redigopack"
	"testing"
)
var redisPool *redigopack.RedisPool

//测试redis的set和get方法
func TestRedisPackClient(t *testing.T)  {
	collectRedis()
	//尝试给aa这个键赋值
	_,err := redigopack.GetRedisClient().String.Set("aa",22211, int64(60)).Result()
	t.Errorf("------err:%v",err)
	//获取aa的值
	aa,err2 := redigopack.GetRedisClient().String.Get("aa").String()
	t.Logf("redis key aa value:%s and err2:%v\n", aa, err2)
	bb,err3 := redigopack.GetRedisClient().String.Set("b",3221).String()
	t.Logf("redis key aa value:%s and err2:%v\n", bb, err3)
	defer redisPool.ConnPool.Close()
}

func TestRedisListOpt(t *testing.T)  {
	collectRedis()
	redigopack.GetRedisClient().List.LPush("ali", 10)
	defer redisPool.ConnPool.Close()
}

//链接redis
func collectRedis()  {
	server := "127.0.0.1:6379"
	pwd := ""//如无密码，请放空字符串
	db := 2//数据库
	//建立redis链接
	redisPool = redigopack.InitRedis(server, pwd, db)
	//获取redis客户端对象
	redigopack.InitRedisClient(redisPool, "pre_")

	//关闭对象dia对象
	//defer redisPool.ConnPool.Close()
}