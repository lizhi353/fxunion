package tests

import (
	"context"
	"gitee.com/lizhi353/fxunion/restyhttp"
	"testing"
)

type EcallMsdResult struct {
	List []EcallMsd `json:"list"`
	Total int       `json:"total"`
}
type EcallMsd struct {
	CallerId int `json:"caller_id"`
	CallerName string `json:"caller_name"`
	AbilityId int `json:"ability_id"`
	AbilityName string `json:"ability_name"`
	BusinessCode int `json:"business_code"`
	BusinessName string `json:"business_name"`
	BusinessEnable int `json:"business_enable"`
	MouldId int `json:"mould_id"`
	MouldContent string `json:"mould_content"`
	MouldName string `json:"mould_name"`
	ChannelId int `json:"channel_id"`
	ChannelName string `json:"channel_name"`
	CalleType int `json:"calle_type"`
	Description string `json:"description"`
	CreateTime string `json:"create_time"`
	UpdateTime string `json:"update_time"`
	UpdateBy string `json:"update_by"`
	AccessKey string `json:"access_key"`
	Enable int `json:"enable"`
	Limit int `json:"limit"`
	MustArrived string `json:"must_arrived"`
}
type BusinessResult struct {
	Code        int            `json:"code"`
	Message     string         `json:"message"`
	BusinessObj EcallMsdResult `json:"businessObj"`

}
func TestHttpReqGet (t *testing.T)  {
	restyhttp.InitRestClient()
	url := "http://localgw.test.cloud.enovatemotors.com/push/business/list"
	body := make(map[string]string, 0)
	body["business_code"] = "1706"
	rusinessResult := &BusinessResult{}
	ctx := context.Background()
	err := restyhttp.GetRestyClient().HttpGetReq(ctx, url, body, rusinessResult)
	t.Errorf("result is info:%+v, err:%v", rusinessResult, err)
}

type MessageObj struct {
	Message string `json:"message"`
}

func TestHttpJson(t *testing.T)  {
	restyhttp.InitRestClient()
	url := "http://127.0.0.1:8080/json"
	body := make(map[string]string, 0)
	body["name"] = "zhangsan"
	ctx := context.Background()
	messageObj := &MessageObj{}
	err := restyhttp.GetRestyClient().HttpGetReq(ctx, url, body, messageObj)
	t.Errorf("result:%v, err:%v \n", messageObj, err)
}

type PostRes struct {
	PageNum int `json:"page_num"`
	PageSize int `json:"page_size"`
	Total int `json:"total"`
	Pages int `json:"pages"`
	List []Cimsi `json:"list"`
}

type Cimsi struct {
	Id string `json:"id"`
	Iccid string `json:"iccid"`
	Imsi string `json:"imsi"`
	Msisdn string `json:"msisdn"`
	TaskId string `json:"task_id"`
	Date int `json:"date"`
	SimState string `json:"sim_state"`
	FlowPackage string `json:"flow_package"`
	Vin  string `json:"vin"`
	TboxSN string `json:"tbox_sn"`
	DetailTip string `json:"detailTip"`
}

func TestHttpPost(t *testing.T)  {
	restyhttp.InitRestClient()
	simgParams := map[string]interface{}{
		"vin": "LB9AB2AC8J0LDN227",
		"pager": map[string]interface{}{
			"order":"desc",
			"pageNum":0,
			"pageSize":10,
		},
	}
	postRes := &PostRes{}
	url := "http://localgw.test.cloud.enovatemotors.com/vum/sim/internal/admin/infos"
	ctx := context.Background()
	err := restyhttp.GetRestyClient().HttpPostReq(ctx, url, simgParams, postRes)
	t.Errorf("ctx-----result: %+v, err:%v", postRes, err)
}