package main

import (
	"context"
	"gitee.com/lizhi353/fxunion/loghoop"
	"gitee.com/lizhi353/fxunion/restyhttp"
	"github.com/gin-gonic/gin"
)

type Result struct {
	Message string `json:"message"`
}

func main()  {
	loghoop.SetUp("ecall")//引入项目名
	restyhttp.InitRestClient()
	r := gin.Default()
    //loghoop.AccessLog()为http中间件
	r.Use(loghoop.AccessLog()).GET("/ping", func(c *gin.Context) {
		body := map[string]string{
			"a":"1",
		}
		ctx := context.Background()
		result := &Result{}
		err := restyhttp.GetRestyClient().HttpGetReq(ctx, "http://127.0.0.1:8080/json", body, result)
		if err != nil {
			panic(err)
		}
		loghoop.Infof("------:he:%v", 111112222)
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.GET("/json", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message":"json to be",
		})
	})
	r.Run() // l
}